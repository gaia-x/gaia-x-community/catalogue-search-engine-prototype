#!/bin/sh

cat <<EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: service-api-doc
spec:
  replicas: 1
  selector: 
    matchLabels:
      app: service-api-doc
  template:
    metadata:
      labels:
        app: service-api-doc
    spec:
      containers:
      - name: service-api-doc
        image: redocly/redoc:latest
        ports:
        - containerPort: 80
        env:
          - name: SPEC_URL
            value: https://${CI_ENVIRONMENT_NAME}.gaia-x-demonstrator.eu/api/v1/info/spec
---
apiVersion: v1
kind: Service
metadata:
  name: service-api-doc
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: service-api-doc
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: ingress-api-doc
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /\$1  # default ingress for k8s is nginx
    kubernetes.io/ingress.class: "nginx"
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
spec:
  tls:
  - hosts:
    - ${CI_ENVIRONMENT_NAME}.gaia-x-demonstrator.eu
    secretName: ingress-tls
  rules:
  - host: ${CI_ENVIRONMENT_NAME}.gaia-x-demonstrator.eu
    http:
      paths:
      - path: /doc/api/?(.*)
        backend:
          serviceName: service-api-doc
          servicePort: 80
EOF
