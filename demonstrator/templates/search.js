function unique(arr) {
    let i,
        len = arr.length,
        out = [],
        obj = {};

    for (i = 0; i < len; i++) {
        obj[arr[i]] = 0;
    }
    for (i in obj) {
        out.push(i);
    }
    return out;
};

$(document).ready(function () {
    $.get("{{ reverse_url('search_grammar') }}").done(function (data) {
        console.debug("got generated grammar from server")
        let parser = peg.generate(data)
        $("#search-query").keyup(function () {
            try {
                $("#search-error").text("")
            } catch (err) {
                $("#search-error").text(err.message)
                console.log(unique(err.expected.map(e => e.text).filter(x => x !== undefined)))
            }
        });
    })
})
