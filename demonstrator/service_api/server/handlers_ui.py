from tornado.web import HTTPError
import http
from tornado.httpclient import AsyncHTTPClient
from typing import Union
from .handlers import return_json
from .handlers_db import DbBaseHandler, NodeLabelsHandler, NodeLabelNamesHandler, SearchHandler, FindHandler
from marshmallow import Schema, fields
import json

################################################ SELECTOR ################################################

class SelectorSchema(Schema):
    field = fields.String()
    options = fields.List(fields.String())
    edge = fields.String(required=False)
    property = fields.String(required=False)


class SelectorsSchema(Schema):
    selectors = fields.List(fields.Nested(SelectorSchema))


class SelectorHandler(DbBaseHandler):
    @return_json
    async def get(self):
        """---
description: '[UI] List of Selector'
responses:
  200:
    description: return list of UI selector
    content: 
      application/json:
        schema: SelectorsSchema
"""
        selectors = []
        labels = await NodeLabelsHandler.get_labels(self.db)
        for label in labels:
            names = await NodeLabelNamesHandler.get_names(self.db, label)
            selector = {'field': label, 'options': names}
            query = "MATCH ()-[r]->(b:{0!s}) return DISTINCT TYPE(r) as rel".format(label)
            res = self.db.run(query).single()
            if res:
              selector['edge'] = res['rel']
            else:
              selector['property'] = 'name'
            selectors.append(selector)
        return SelectorsSchema().dump({'selectors': selectors})

################################################ SEARCH ################################################

class UINodeSchema(Schema):
    id = fields.Integer(required=True)
    labels = fields.List(fields.String(), required=True)
    properties = fields.Mapping(keys=fields.String())


class UISearchResultSchema(Schema):
    query = fields.String()
    nodes = fields.List(fields.Nested(UINodeSchema))


class UISearchHandler(DbBaseHandler):
    async def search(self, query):
        query, nodes = await SearchHandler.search(self.db, query)
        for idx in range(len(nodes)):
            if 'Service' in nodes[idx]['labels']:
                print(nodes[idx]['id'])
                _, providers = await FindHandler.find(self.db, 'Provider', nodes[idx]['id'])
                if len(providers):
                    nodes[idx]['properties']['provider'] = providers[0]
        return UISearchResultSchema().dump({'query': query, 'nodes': nodes})

    @return_json
    async def post(self):
        """---
description: '[UI] Database query'
parameters:
  - in: body
    name: query
    schema:
      type: object
    description: object as returned by the PEG parser
responses:
  200:
    description: return list of database node
    content:
      application/json:
        schema: UISearchResultSchema
"""
        if self.request.headers['Content-Type'] != 'application/json':
            raise HTTPError(http.client.BAD_REQUEST, "unknown content-type. Expecting 'application/json'")
        try:
            args = json.loads(self.request.body)
            query = args.get('query', None)
            if not query:
              raise HTTPError(http.client.BAD_REQUEST, "missing query parameter 'query'")
            query = json.dumps(query) # convert it back to string because it'll be deserialized later with custom class
            return await self.search(query)
        except TypeError as ex:
            raise HTTPError(http.client.BAD_REQUEST, "wrong query format: {}".format(ex))

    @return_json
    async def get(self):
        """---
description: '[UI] Database query'
parameters:
  - in: query
    name: q
    schema:
      type: string
    description: object as returned by the PEG parser
responses:
  200:
    description: return list of database node
    content:
      application/json:
        schema: UISearchResultSchema
"""
        query = self.get_argument("q", None)
        if not query:
            raise HTTPError(http.client.BAD_REQUEST, "missing query parameter 'q'")
        return await self.search(query)
