import json
from .handlers import BaseTest
import tornado.testing
from unittest.mock import Mock, patch
import sys
import http.client
from tornado.httpclient import HTTPError
from server.db import GraphDB
from urllib.parse import urlencode
from server.rules import Attribute, Relation, And, Or
import logging


logger = logging.getLogger("tornado.application")
logger.setLevel(logging.DEBUG)


def query_to_json(query):
    class _encoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, Attribute): # since Attribute inherit from Relation, we need to keep this test order
                return dict([(k, obj.__getattribute__(k)) for k in ['node', 'key', 'verb', 'value', 'neg']])
            if isinstance(obj, Relation):
                return dict([(k, obj.__getattribute__(k)) for k in ['node', 'verb', 'value', 'neg']])
            if isinstance(obj, Or):
                return {'or': obj.exprs}
            if isinstance(obj, And):
                return {'and': obj.exprs}
            return json.JSONEncoder.default(self, obj)
    return json.dumps(query, cls=_encoder)


class GrammarTest(BaseTest):
    @tornado.testing.gen_test
    async def test_retreive_grammar(self):
        with patch('server.db.GraphDB', autospec=True) as db:
            response = await self.http_client.fetch(self.reverse_url('db.grammar'))
            db.assert_called_once()
        self.assertEqual(response.code, 200)
        headers = tornado.httputil.HTTPHeaders(response.headers)
        self.assertIn('content-type', headers)
        self.assertEqual(headers['content-type'], 'text/javascript')


class SearchTest(BaseTest):
    @tornado.testing.gen_test
    async def test_no_query(self):
        with self.assertRaises(HTTPError) as cm:
            await self.http_client.fetch(self.reverse_url('db.search'))
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)
        with self.assertRaises(HTTPError) as cm:
            await self.http_client.fetch(self.reverse_url('db.search') + '?' + urlencode({'query': "something"}))
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @tornado.testing.gen_test
    async def test_query_wrong_format(self):
        queries = ["something", {}, {"foo": "bar"},
            {'node': 'Latter', 'key': 'type', 'verb': '=', 'value': 'a'},
            {'node': 'Letter', 'key': 'type', 'verb': '~=', 'value': 'a', 'neg': False},
            {'node': 'Letter', 'key': 'type', 'verb': '=', 'value': [], 'neg': "4"},
            {'node': 'Letter', 'rel': {}, 'value': 'a'},
            {'node': 'Letter', 'rel': 'IN', 'value': 'a', 'extra': '42'}
        ]
        for query in queries:
            with self.assertRaises(HTTPError) as cm:
                response = await self.http_client.fetch(self.reverse_url('db.search') + '?' + urlencode({'q': json.dumps(query)}))
                logger.error(response.body)
            self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    async def _test_queries(self, queries):
        for query, count in queries:
            logger.info("--> {}".format(query))
            response = await self.http_client.fetch(self.reverse_url('db.search') + '?' + urlencode({'q': query_to_json(query)}))
            self.assertEqual(response.code, 200)
            objects = json.loads(response.body)
            logger.info("<-- {}".format(objects['query']))
            logger.debug(objects['nodes'])
            self.assertEqual(len(objects['nodes']), count)


    @tornado.testing.gen_test
    async def test_query_relation(self):
        queries = [
            (Relation("Letter", "IN", "Alphabet"), 3),
            (Relation("Symbol", "IN", "Alphabet", neg=True), 2),
        ]
        await self._test_queries(queries)

    @tornado.testing.gen_test
    async def test_query_attribute(self):
        queries = [
            (Attribute("Letter", "name", "=", "A"), 1),
            (Attribute("Letter", "name", "!=", "A"), 2),
            (Attribute("Letter", "name", "!=", "A", True), 1),
        ]
        await self._test_queries(queries)

    @tornado.testing.gen_test
    async def test_query_logic_or(self):
        queries = [
            (Or(Relation("Letter", "IN", "Alphabet"), Relation("Integer", "IS", "all")), 5),
            (Or(Relation("Letter", "IN", "Alphabet")), 3),
            (Or(Relation("Letter", "IN", "Alphabet"), Relation("Letter", "IN", "Alphabet", neg=True)), 3),
            (Or(Attribute("Letter", "name", "=", "A"), Attribute("Integer", "name", "=", "1")), 2)
        ]
        await self._test_queries(queries)
        
    @tornado.testing.gen_test
    async def test_query_logic_and(self):
        queries = [
            (And(Relation("Letter", "IN", "Alphabet"), Relation("Letter", "IS", "all")), 3),
            (And(Relation("Letter", "IN", "Alphabet"), Relation("Integer", "IS", "all")), 0),
            (And(Relation("Letter", "IN", "Alphabet"), Relation("Letter", "IN", "Alphabet", neg=True)), 0),
        ]
        await self._test_queries(queries)


class NodeLabelsTest(BaseTest):
    @tornado.testing.gen_test
    async def test_label(self):
        response = await self.http_client.fetch(self.reverse_url('db.node'))
        objects = json.loads(response.body)
        self.assertDictEqual(objects, {"labels": ["Letter", "Symbol", "Group", "Integer", "All", "Service", "Provider"]})


class NodeLabelNamesTest(BaseTest):
    @tornado.testing.gen_test
    async def test_wrong_label(self):
        with self.assertRaises(HTTPError) as cm:
            await self.http_client.fetch(self.reverse_url('db.node.label', 'foo'))
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @tornado.testing.gen_test
    async def test_label(self):
        response = await self.http_client.fetch(self.reverse_url('db.node.label', 'Letter'))
        objects = json.loads(response.body)
        self.assertDictEqual(objects, {'label': 'Letter', "names": ["A", "B", "C"]})


class EdgeTypesTest(BaseTest):
    @tornado.testing.gen_test
    async def test_label(self):
        response = await self.http_client.fetch(self.reverse_url('db.edge'))
        objects = json.loads(response.body)
        self.assertDictEqual(objects, {"types": ["IN", "IS", "HOSTED_BY"]})


class EdgeTypeLabelsTest(BaseTest):
    @tornado.testing.gen_test
    async def test_wrong_label(self):
        with self.assertRaises(HTTPError) as cm:
            await self.http_client.fetch(self.reverse_url('db.edge.type', 'foo'))
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @tornado.testing.gen_test
    async def test_label(self):
        response = await self.http_client.fetch(self.reverse_url('db.edge.type', 'IN'))
        objects = json.loads(response.body)
        self.assertDictEqual(objects, {'type': 'IN', "labels": ["Group"]})


class FindTest(BaseTest):
    @tornado.testing.gen_test
    async def test_wrong_label(self):
        with self.assertRaises(HTTPError) as cm:
            await self.http_client.fetch(self.reverse_url('db.find', 'foo'))
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @tornado.testing.gen_test
    async def test_wrong_id(self):
        with self.assertRaises(HTTPError) as cm:
            await self.http_client.fetch(self.reverse_url('db.find', 'Provider') + '?' + urlencode({'id': '#'}))
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)
        with self.assertRaises(HTTPError) as cm:
            await self.http_client.fetch(self.reverse_url('db.find', 'Provider') + '?' + urlencode({'id': -1}))
        self.assertEqual(cm.exception.code, http.client.BAD_REQUEST)

    @tornado.testing.gen_test
    async def test_simple(self):
        query = Attribute("Letter", "name", "=", "A")
        response = await self.http_client.fetch(self.reverse_url('db.search') + '?' + urlencode({'q': query_to_json(query)}))
        self.assertEqual(response.code, 200)
        objects = json.loads(response.body)
        logger.debug(objects['nodes'])
        self.assertEqual(len(objects['nodes']), 1)
        node = objects['nodes'][0]
        logger.debug(node)
        response = await self.http_client.fetch(self.reverse_url('db.find', 'Group') + '?' + urlencode({'id': node['id']}))
        self.assertEqual(response.code, 200)
        objects = json.loads(response.body)
        self.assertTrue(all(['Group' in node['labels'] for node in objects['nodes']]))

