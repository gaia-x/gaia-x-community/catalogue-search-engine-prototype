const peg = require("pegjs");
const axios = require('axios');
const http = require('http');
var url = require('url');

let parser = undefined;

const requestListener = async function (req, res) {
    let q = url.parse(req.url, true).query;
    let grammar_url = q.url;
    let query = q.query
    if (parser === undefined) {
      let grammar = await axios.get(grammar_url);
      parser = peg.generate(grammar.data);
    }
    try {
      let obj = parser.parse(query)
      res.writeHead(200, {'Content-Type': 'application/json'});
      res.end(JSON.stringify(obj));
    } catch (err) {
      res.writeHead(400);
      res.end(JSON.stringify(err));
      console.error("query=" + query)
      console.error("error=" + err)
    }
}

const server = http.createServer(requestListener);
server.listen(0, function() {
    process.stdout.write(server.address().port + "\n");
});
