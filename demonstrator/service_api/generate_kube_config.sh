#!/bin/sh

cat <<EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  name: service-api
spec:
  replicas: 1
  selector: 
    matchLabels:
      app: service-api
  template:
    metadata:
      labels:
        app: service-api
    spec:
      imagePullSecrets:
      - name: ci-registry
      containers:
      - name: service-api
        image: ${CI_REGISTRY_IMAGE}/service_api:${CI_COMMIT_REF_SLUG}-latest
        imagePullPolicy: Always
        ports:
        - containerPort: 8888
        env:
          - name: NEO4J_USERNAME
            value: neo4j
          - name: NEO4J_PASSWORD
            valueFrom:
              secretKeyRef:
                name: db-neo4j-secrets
                key: neo4j-password
          - name: NEO4J_ADDRESS
            value: bolt://db-neo4j
          - name: SERVER_URL
            value: https://${CI_ENVIRONMENT_NAME}.gaia-x-demonstrator.eu/api/v1/
---
apiVersion: v1
kind: Service
metadata:
  name: service-api
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 8888
  selector:
    app: service-api
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: ingress-api
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /\$1  # default ingress for k8s is nginx
    kubernetes.io/ingress.class: "nginx"
    cert-manager.io/cluster-issuer: "letsencrypt-prod"
spec:
  tls:
  - hosts:
    - ${CI_ENVIRONMENT_NAME}.gaia-x-demonstrator.eu
    secretName: ingress-tls
  rules:
  - host: ${CI_ENVIRONMENT_NAME}.gaia-x-demonstrator.eu
    http:
      paths:
      - path: /api/v1/?(.*)
        backend:
          serviceName: service-api
          servicePort: 80
EOF
