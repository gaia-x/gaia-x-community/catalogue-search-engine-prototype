import React from 'react'
import styled from '@xstyled/styled-components'

import Header from 'sections/Header'
import List from 'sections/List'

function App() {
  return (
    <Container>
      <Header />
      <List />
    </Container>
  )
}

const Container = styled.div`
  height: 100vh;
  background-color: white;
`

export default App
