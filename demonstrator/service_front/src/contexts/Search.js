import React, { useState, useContext, useCallback } from 'react'
import api from 'api'
import useApi from 'hooks/useApi'

const defaultValue = {
  nodes: {},
  loadNodes: () => {},
  relation: '',
  setRelation: () => {},
  status: 'pending',
}

const SearchContext = React.createContext(defaultValue)
const SearchProvider = ({ children }) => {
  const [nodes, setNodes] = useState({})
  const [relation, setRelation] = useState('')

  const loadNodes = useCallback(
    async (submittedRelation) => {
      const res = await api.search.get(relation || submittedRelation)
      setNodes(res)
    },
    [relation]
  )

  const [, { status, load }] = useApi({
    load: loadNodes,
    initialData: [],
    autoLoad: false,
  })

  return (
    <SearchContext.Provider
      value={{
        nodes,
        loadNodes: load,
        relation,
        setRelation,
        status,
      }}
    >
      {children}
    </SearchContext.Provider>
  )
}

export const useSearch = () => useContext(SearchContext)
export default SearchProvider
