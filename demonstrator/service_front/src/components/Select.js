import React, { useState, useCallback } from 'react'
import Checkbox from '@material-ui/core/Checkbox'
import TextField from '@material-ui/core/TextField'
import Chip from '@material-ui/core/Chip'
import Popper from '@material-ui/core/Popper'
import Autocomplete from '@material-ui/lab/Autocomplete'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank'
import CheckBoxOutlinedIcon from '@material-ui/icons/CheckBoxOutlined'
import { withStyles } from '@material-ui/core/styles'

import theme from 'helpers/theme'
import computeUniqID from 'helpers/computeUniqID'
import agent from 'helpers/getUserAgent'

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />
const checkedIcon = <CheckBoxOutlinedIcon fontSize="small" color="primary" />

const StyledTextField = withStyles({
  root: {
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: theme.colors.mischka,
      },
      '&:hover fieldset': {
        borderColor: theme.colors.primary,
      },
      '&.Mui-focused fieldset': {
        borderColor: theme.colors.primary,
        borderWidth: 1,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
      },
    },
  },
})(TextField)

const StyledAutocomplete = withStyles({
  popper: {
    '& .MuiAutocomplete-paper': {
      borderTopLeftRadius: 0,
      borderTopRightRadius: 0,
      borderTop: 'unset',
      border: `1px ${theme.colors.primary} solid`,
      marginTop: 0,
    },
    '& .MuiAutocomplete-listbox': {
      padding: 0,
    },
    '& .MuiAutocomplete-option': {
      padding: 0,
      fontSize: '0.8rem',
      textOverflow: 'ellipsis',
      display: 'block',
      overflow: 'hidden',
      whiteSpace: 'nowrap',

      '& > span': {
        padding: '5px 5px 5px 8px',
      },
      '& svg': {
        fontSize: '1rem',
      },
    },
    '& .MuiAutocomplete-option[aria-selected=true]': {
      color: theme.colors.primary,
    },
  },
})(Autocomplete)

const popperOffset = {
  CHROME: {
    base: '2px',
    withValue: '0px',
  },
  SAFARI: {
    base: '0px',
    withValue: '0px',
  },
  FIREFOX: {
    base: '0px',
    withValue: '0px',
  },
}

export default function Select({
  value = [],
  label = 'Input',
  options = [],
  handleOnChange = () => {},
  multiple = false,
}) {
  const [gotFocus, focus] = useState(false)
  const onFocus = useCallback(() => focus(true), [focus])

  const onChange = useCallback(
    (values = []) => {
      if (!gotFocus && values.length) onFocus(true)
      handleOnChange(values)
      console.log(values)
    },
    [gotFocus, onFocus, handleOnChange]
  )

  return (
    <StyledAutocomplete
      debug
      value={value}
      id={`select-${computeUniqID()}`}
      multiple={multiple}
      limitTags={1}
      options={options}
      disableCloseOnSelect={multiple}
      getOptionLabel={(option) => option.title}
      onChange={(...res) => {
        onChange(res[1])
      }}
      renderTags={(tags = []) => (
        <Chip size="small" color="primary" label={`+${tags.length}`} />
      )}
      style={{ width: 150 }}
      size="small"
      renderOption={(option, { selected }) => (
        <>
          {multiple && (
            <Checkbox
              icon={icon}
              checkedIcon={checkedIcon}
              style={{ marginRight: 8 }}
              checked={selected}
            />
          )}
          {option.title}
        </>
      )}
      renderInput={(params) => (
        <StyledTextField
          {...params}
          variant="outlined"
          label={label}
          onFocus={onFocus}
        />
      )}
      openOnFocus
      PopperComponent={(props) => (
        <Popper
          {...props}
          modifiers={{
            offset: {
              enabled: true,
              offset: gotFocus
                ? popperOffset[agent].withValue
                : popperOffset[agent].base,
            },
          }}
        />
      )}
    />
  )
}
