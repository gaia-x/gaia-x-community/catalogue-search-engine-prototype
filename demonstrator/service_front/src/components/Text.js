import styled, { Box, css } from '@xstyled/styled-components'

const variants = {
  h1: css`
    font-size: 2.2rem;
    line-height: 24px;
  `,
  h2: css`
    font-size: 1.8rem;
    line-height: 20px;
  `,
  h3: css`
    font-size: 1.5rem;
    line-height: 18px;
  `,
  default: css`
    font-size: 1.2rem;
    line-height: 8px;
  `,
}

export default styled(Box)`
  ${({ $variant = 'default' }) => variants[$variant]}
`
