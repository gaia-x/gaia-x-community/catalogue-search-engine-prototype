import React from 'react'
import { createGlobalStyle } from '@xstyled/styled-components'

const Styled = createGlobalStyle`
  html,
  body {
    font-family: 'Open Sans', sans-serif;
    -webkit-tap-highlight-color: transparent;
    text-rendering: optimizeLegibility;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  html {
    line-height: 1.5;
    height: 100%;
    margin: 0;
  }

  body {
    font-family: 'Open Sans', sans-serif;
    background-color: white;
    color: secondary;
    font-size: 1.6rem;
    height: 100%;
    margin: 0;
  }

  code {
  font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
    monospace;
}
`

export default function GlobalStyle() {
  return <Styled />
}
