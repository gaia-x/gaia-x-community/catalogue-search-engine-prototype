import React from 'react'

import styled, { Box, css } from '@xstyled/styled-components'
import theme from 'helpers/theme'

export const Table = ({ columns, children }) => {
  return <TableContainer $columns={columns}>{children}</TableContainer>
}
const Header = ({ columns, onSort = () => {} }) => {
  return (
    <HeaderContainer className="row">
      {columns.map((column, index) => (
        <HeaderCell
          key={index}
          onClick={column.sort ? () => onSort(index) : undefined}
          style={{ cursor: column.sort ? 'pointer' : 'default' }}
          className="cell"
        >
          <Box as="span">{column.label}</Box>
          {/* {column.sort && (
            <SortIcon active={sortedIndex === index} order={sortOrder} />
          )} */}
        </HeaderCell>
      ))}
    </HeaderContainer>
  )
}

const Cell = ({ children, ...props }) => <Box className="cell" {...props}>{children}</Box>
const Row = ({ children, ...props }) => (
  <RowContainer className="row" {...props}>
    {children}
  </RowContainer>
)
const Body = ({ data, children }) => {
  return (
    <BodyContainer>
      {(data || []).map((item, index) => (
        <React.Fragment key={index}>
          {children({ data: item, Row, Cell })}
        </React.Fragment>
      ))}
    </BodyContainer>
  )
}

Table.Header = Header
Table.Body = Body

export default Table

const TableContainer = styled.div`
  width: 100%;
  .row {
    display: flex;
    align-items: center;
    flex-wrap: wrap;

    .cell {
      ${({ $columns = [] }) => css`
        ${$columns.map(
          (column, index) =>
            `&:nth-of-type(${index + 1}) {
            align-items: center;
            ${column.width ? `width :${column.width};` : 'flex : 1;'}
            ${column.padding ? `padding: ${column.padding};` : ''}
            ${
              column.justifyContent
                ? `justify-content: ${column.justifyContent};`
                : ''
            }
          }`
        )}
      `}
    }
  }
`

const HeaderContainer = styled.div`
  width: 100%;
  margin-bottom: 16px;
`
const HeaderCell = styled.div`
  color: mischka;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  font-weight: 600;
  color: blueHaze;
  font-size: 1.1rem;

  :first-child {
    span {
      padding-left: 16px;
    }
  }
`

const BodyContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

const RowContainer = styled(Box)`
  padding: 16px;
  border: 1px solid transparent;
  border-color: blueHaze;
  border-radius: 8px;
  margin-bottom: 16px;
  transition: box-shadow 350ms ease;
  font-size: 1rem;
  :hover {
    box-shadow: 0px 0px 3px 4px ${theme.colors.hawkesBlue};
  }
`
