import React from 'react'
import ReactDOM from 'react-dom'
import { ThemeProvider } from '@xstyled/styled-components'
import { ThemeProvider as MUIThemeProvider } from '@material-ui/styles'

import GlobalStyle from 'components/GlobalStyle'
import SearchProvider from 'contexts/Search'

import theme, { MUITheme } from 'helpers/theme'
import App from './App'

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <MUIThemeProvider theme={MUITheme}>
        <GlobalStyle />
        <SearchProvider>
          <App />
        </SearchProvider>
      </MUIThemeProvider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
)
