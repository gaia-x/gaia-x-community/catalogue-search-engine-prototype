import peg from 'pegjs'
import api from 'api'
import useApi from 'hooks/useApi'

export let parser = {}
export default function () {
  let loading = true
  const [data, { error: apiError }] = useApi({ load: api.grammar.get })
  if (apiError) return { isLoading: false, hasError: true }

  if (typeof data === 'string') {
    try {
      parser = peg.generate(data)
      loading = false
    } catch (err) {
      return { isLoading: false, hasError: true }
    }
  }
  return { isLoading: loading, hasError: false }
}
