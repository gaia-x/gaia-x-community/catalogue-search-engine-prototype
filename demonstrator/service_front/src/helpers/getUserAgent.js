const agents = [
  ['Chrome', 'CHROME'],
  ['Safari', 'SAFARI'],
  ['OP', 'OPERA'],
  ['Firefox', 'FIREFOX'],
  ['MSIE', 'IE'],
  ['rv:', 'IE'],
]

function getAgent() {
  const { userAgent: systemUserAgent } = navigator || {}

  const [first = '', second = ''] = agents
    .map(([agent, label]) => systemUserAgent.indexOf(agent) > -1 && label)
    .filter(Boolean)

  let final = first || 'CHROME'
  if (first === 'CHROME' && second === 'SAFARI') final = first
  if (first === 'CHROME' && second === 'OPERA') final = second

  return final
}

const agent = getAgent()
export default agent
