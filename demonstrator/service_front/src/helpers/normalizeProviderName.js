export function normalizeProviderName(name = '') {
  return name.trim().replace(/ /g, '').toLowerCase()
}
