export const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms))

export const requestAnimationFrame = () => new Promise(requestAnimationFrame)

export const sleepForeground = async (ms) =>
  sleep(ms).then(requestAnimationFrame)

export const sleepForegroundLoop = (ms, fn) => {
  let stop = false
  const stopLoop = () => {
    stop = true
  }
  /* eslint-disable */
  ;(async () => {
    while (true) {
      await sleepForeground(ms)
      if (stop) return
      fn()
    }
  })()
  return { stop: stopLoop }
}
