export default (obj, ...paths) =>
  paths
    .join('.')
    .split('.')
    .reduce((acc, key) => acc && acc[key], obj)
