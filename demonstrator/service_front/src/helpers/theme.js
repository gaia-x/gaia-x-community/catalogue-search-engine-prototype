import { createMuiTheme } from '@material-ui/core/styles'

const colors = {
  primary: '#5884F1',
  secondary: '#565D78',
  lynch: '#717997',

  hawkesBlue: '#EEF3FE',
  athensGray: '#F6F7F9',
  mischka: '#CFD3E1',
  blueHaze: '#C4C9DB',
  emerald: '#63C380',
  roman: '#D7544F',
}
export default {
  colors: {
    ...colors,
  },
  space: [0, 8, 16, 24, 32, 40, 48, 56, 64, 72],
}

export const MUITheme = createMuiTheme({
  typography: {
    fontFamily: 'Open Sans, sans-serif',
  },
  palette: {
    primary: {
      main: colors.primary,
    },
    secondary: {
      main: colors.secondary,
    },
  },
})
