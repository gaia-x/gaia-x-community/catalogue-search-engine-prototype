import { useCallback } from 'react'
import { parser } from 'parser'

function useParser() {
  const parse = useCallback(({ string, onlyError = false }) => {
    try {
      const res = parser.parse(string)
      return {
        relation: res,
        error: null,
      }
    } catch (err) {
      return onlyError ? err : { relation: null, error: err }
    }
  }, [])

  return {
    parse,
  }
}
export default useParser
