import { useEffect, useCallback } from 'react'
import { throttle } from 'throttle-debounce'

function useScrollPosition(onScroll) {
  const cb = useCallback(() => {
    onScroll({ scroll: window.scrollY })
  }, [onScroll])

  const handle = useCallback(throttle(100, cb), [])

  useEffect(() => {
    window.addEventListener('scroll', handle)
    window.addEventListener('load', handle)
    return () => {
      window.removeEventListener('scroll', handle)
      window.removeEventListener('load', handle)
    }
  }, [handle])
}

export default useScrollPosition
