import { useState, useCallback, useEffect, useRef } from 'react'
import { sleepForegroundLoop } from 'helpers/promise'

function useApi({
  initialData = {},
  load = () => {},
  autoLoad = true,
  polling = false,
  pollingOptions: { interval = 10000, shouldRefresh = () => true } = {},
}) {
  const [data, setData] = useState(initialData)
  const [status, setStatus] = useState(autoLoad ? 'loading' : 'pending')
  const [error, setError] = useState(null)
  const isUmount = useRef(false)

  const request = useCallback(
    async (params) => {
      setStatus('loading')

      try {
        if (isUmount.current) return
        const loadedData = await load(params)
        setData(loadedData)
        setStatus('success')
      } catch (err) {
        if (isUmount.current) return
        setError(err)
        setStatus('error')
      }
    },
    [load, isUmount]
  )

  useEffect(() => {
    isUmount.current = false
    if (autoLoad) {
      request()
    }
    return () => {
      isUmount.current = true
    }
  }, [autoLoad, request])

  useEffect(() => {
    if (!polling) {
      return undefined
    }
    const handler = sleepForegroundLoop(interval, () => {
      if (shouldRefresh(status)) {
        request()
      }
    })
    return handler.stop
  }, [interval, polling, request, shouldRefresh, status])

  return [
    data,
    {
      status,
      error,
      isLoading: status === 'loading',
      load: request,
      httpStatus: (data || {}).status,
    },
  ]
}

export default useApi
