import client from './client'

export default {
  get: async function () {
    const res = (await client.get('/ui/selector')) || {}
    return (res.data || {}).selectors || []
  },
}
