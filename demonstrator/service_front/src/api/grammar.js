import client from './client'

export default {
  get: async function () {
    const res = (await client.get('/db/grammar')) || {}
    return res.data
  },
}
