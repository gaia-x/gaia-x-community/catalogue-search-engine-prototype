import * as axios from 'axios'

const BASE_CONFIG = {
  timeout: 30000,
  headers: {
    common: {
      Accept: 'application/json',
    },
    get: {
      'Content-Type': 'application/json',
    },
    post: {
      'Content-Type': 'application/json',
    },
  },
}

const client = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
  ...BASE_CONFIG,
})
export default client
