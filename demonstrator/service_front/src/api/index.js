import grammar from './grammar'
import search from './search'
import selectors from './selectors'

export default {
  grammar,
  search,
  selectors,
}
