import client from './client'

export default {
  get: async function (relation = '') {
    const res = (await client.get(`/ui/search?q=${relation}`)) || {}
    return res.data
  },
}
