import React from 'react'
import styled, { Box, css, keyframes } from '@xstyled/styled-components'
import CircularProgress from '@material-ui/core/CircularProgress'

import { useSearch } from 'contexts/Search'

import Table from 'components/Table'
import Text from 'components/Text'

import ToTop from './ToTop'
import { heads, rows, formatData } from './types'

function reduceNodesByLabels(nodes = []) {
  return nodes.reduce((acc, { labels = [], properties = {} } = {}) => {
    let label = ''
    if (labels.includes('Service')) {
      label = 'Service'
    } else {
      label = labels[0]
    }

    const item = acc[label]

    if (item) {
      return {
        ...acc,
        [label]: [
          ...item,
          {
            ...formatData[label]({ properties }),
          },
        ],
      }
    }
    return {
      ...acc,
      [label]: [
        {
          ...formatData[label]({ properties }),
        },
      ],
    }
  }, {})
}

function List() {
  const { nodes: { nodes = [] } = {}, status } = useSearch()
  const reducedNodes = reduceNodesByLabels(nodes)

  return (
    <Container id="section-list">
      {status === 'pending' ? null : status === 'loading' ||
        status === 'error' ? (
        <Box display="flex" justifyContent="center">
          <CircularProgress />
        </Box>
      ) : (
        <Content>
          <ToTop />
          {Object.entries(reducedNodes).map(([label, data]) => {
            return (
              <div key={label}>
                <Text as="p" $variant="h3" color="lynch">
                  {label}
                </Text>
                <Table columns={heads[label]}>
                  <Table.Header columns={heads[label]}></Table.Header>
                  <Table.Body data={data}>{rows[label]}</Table.Body>
                </Table>
              </div>
            )
          })}
        </Content>
      )}
    </Container>
  )
}

const Container = styled.section`
  display: flex;
  flex-direction: column;
  height: auto;
  margin: 0 auto;
  position: relative;
  width: auto;
  max-width: min-content;
  padding: 0 16 16px;
  width: 100%;
  max-width: 970px;
  margin-top: -24px;
  flex: 1;
`

const fadeIn = keyframes`
  0% {
      opacity: 0;
  }
  100% {
    opacity: 1;
  }
`
const Content = styled.div`
  animation: ${() =>
    css`
      ${fadeIn} 800ms ease forwards
    `};
`

export default List
