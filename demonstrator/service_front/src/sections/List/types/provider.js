import React from 'react'
import styled from '@xstyled/styled-components'

import Button from '@material-ui/core/Button'

import docaposteLogo from 'assets/docaposte.svg'
import innovocloudLogo from 'assets/innovocloud.png'
import opentelekomcloudLogo from 'assets/opentelekomcloud.svg'
import orangebusinessservicesLogo from 'assets/orangebusinessservices.svg'
import outscaleLogo from 'assets/outscale.png'
import ovhcloudLogo from 'assets/ovhcloud.svg'
import scalewayLogo from 'assets/scaleway.svg'

import { normalizeProviderName } from 'helpers/normalizeProviderName'

const NAME = 'Provider'

const logos = {
  docaposte: docaposteLogo,
  innovocloud: innovocloudLogo,
  opentelelomcloud: opentelekomcloudLogo,
  orangebusinessservices: orangebusinessservicesLogo,
  outscale: outscaleLogo,
  ovhcloud: ovhcloudLogo,
  scaleway: scalewayLogo,
}

export const head = {
  [NAME]: [
    {
      label: 'Cloud Provider',
      width: '150px',
    },
    {
      label: '',
      width: '220px',
    },
    {
      label: 'Created in',
    },
    {
      label: '',
      width: '100px',
    },
  ],
}

export const formatData = {
  [NAME]: ({ properties = {} }) => ({
    cp: properties.name,
    created: properties.created_in,
    url: properties.website,
  }),
}

export const row = {
  [NAME]: ({ Row, Cell, data }) => (
    <Row>
      <Cell fontWeight={700}>{data.cp}</Cell>
      <Cell>
        {logos[normalizeProviderName(data.cp)] ? (
          <Logo src={logos[normalizeProviderName(data.cp)]} alt={data.cp} />
        ) : null}
      </Cell>
      <Cell>{data.created}</Cell>
      <Cell>
        <Button
          variant="contained"
          color="primary"
          href={data.url}
          target="_blank"
          rel="noopener noreferrer"
        >
          See more
        </Button>
      </Cell>
    </Row>
  ),
}

const Logo = styled.img`
  width: 100px;
`
