import React from 'react'
import styled from '@xstyled/styled-components'

import Button from '@material-ui/core/Button'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import CancelIcon from '@material-ui/icons/Cancel'

import theme from 'helpers/theme'
import { normalizeProviderName } from 'helpers/normalizeProviderName'

import docaposteLogo from 'assets/docaposte.svg'
import innovocloudLogo from 'assets/innovocloud.png'
import opentelekomcloudLogo from 'assets/opentelekomcloud.svg'
import orangebusinessservicesLogo from 'assets/orangebusinessservices.svg'
import outscaleLogo from 'assets/outscale.png'
import ovhcloudLogo from 'assets/ovhcloud.svg'
import scalewayLogo from 'assets/scaleway.svg'

const checkedIcon = <CheckCircleIcon style={{ color: theme.colors.emerald }} />
const errorIcon = <CancelIcon style={{ color: theme.colors.roman }} />

const NAME = 'Service'

const logos = {
  docaposte: docaposteLogo,
  innovocloud: innovocloudLogo,
  opentelelomcloud: opentelekomcloudLogo,
  orangebusinessservices: orangebusinessservicesLogo,
  outscale: outscaleLogo,
  ovhcloud: ovhcloudLogo,
  scaleway: scalewayLogo,
}

export const head = {
  [NAME]: [
    {
      label: 'Product name',
      width: '200px',
    },
    {
      label: 'Stack',
    },
    {
      label: 'High Availability',
    },
    {
      label: 'Security',
    },
    {
      label: 'Cloud Provider',
    },
    {
      label: '',
      width: '100px',
    },
  ],
}

export const formatData = {
  [NAME]: ({ properties: { provider = {}, ...service } } = {}) => ({
    name: service.name,
    stack: service.backend_stack,
    ha: service.high_availabity,
    security: service.security,
    cp: (provider.properties || {}).name || '',
    url: service.website,
  }),
}

export const row = {
  [NAME]: ({ Row, Cell, data }) => (
    <Row>
      <Cell fontWeight={700}>{data.name}</Cell>
      <Cell>{data.stack}</Cell>
      <Cell>{data.ha ? checkedIcon : errorIcon}</Cell>
      <Cell>{data.security || 'N/A'}</Cell>
      <Cell>
        {logos[normalizeProviderName(data.cp)] ? (
          <Logo src={logos[normalizeProviderName(data.cp)]} alt={data.cp} />
        ) : (
          data.cp
        )}
      </Cell>
      <Cell>
        <Button
          variant="contained"
          color="primary"
          href={data.url}
          target="_blank"
          rel="noopener noreferrer"
        >
          See more
        </Button>
      </Cell>
    </Row>
  ),
}

const Logo = styled.img`
  width: 100px;
`
