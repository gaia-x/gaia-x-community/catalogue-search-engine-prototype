import * as service from './service'
import * as provider from './provider'

export const heads = {
  ...service.head,
  ...provider.head,
}
export const formatData = {
  ...service.formatData,
  ...provider.formatData,
}
export const rows = {
  ...service.row,
  ...provider.row,
}
