import React, { useState, useCallback } from 'react'
import { scroller } from 'react-scroll'
import styled, { keyframes, css } from '@xstyled/styled-components'
import ExpandLessIcon from '@material-ui/icons/ExpandLess'

import useScrollPosition from 'hooks/useScrollPosition'
import theme from 'helpers/theme'

function ToTop() {
  const [show, setShow] = useState(false)
  const [animated, setAnimated] = useState(false)

  useScrollPosition(
    useCallback(
      ({ scroll }) => {
        if (scroll > 440) setShow(true)
        if (scroll < 440) setShow(false)
      },
      [setShow]
    )
  )

  if (show && !animated) setAnimated(true)

  const scrollToTop = useCallback(() => {
    scroller.scrollTo('section-search', {
      duration: 800,
      smooth: true,
      offset: -150,
    })
  }, [])
  return (
    <Container show={show} animated={animated} onClick={scrollToTop}>
      <Icon>
        <ExpandLessIcon style={{ color: theme.colors.blueHaze }} />
      </Icon>
    </Container>
  )
}

const fadeIn = keyframes`
  0% {
      opacity: 0;
  }
  100% {
    opacity: 1;
  }
`

const fadeOut = keyframes`
  0% {
      opacity: 1;
  }
  99% {
    opacity: 0;
  }
  100% {
    pointer-events: none;
    opacity: 0;
  }
`

const upAndDown = keyframes`
  0% {
    transform: translate3d(0px, 3px, 0px);
  }
  100% {
    transform: translate3d(0px, -3px, 0px);
  }
`
const Container = styled.div`
  display: flex;
  justify-content: center;
  position: absolute;
  width: 100%;
  top: -20px;
  opacity: 0;

  ${({ show }) =>
    show &&
    css`
      animation: ${() =>
        css`
          ${fadeIn} 3s ease forwards
        `};
    `}
  ${({ show, animated }) =>
    !show &&
    animated &&
    css`
      opacity: 1;
      animation: ${() =>
        css`
          ${fadeOut} 3s ease forwards
        `};
    `}
  :hover {
    cursor: pointer;
  }
`

const Icon = styled.div`
  animation: ${() =>
    css`
      ${upAndDown} 2s ease infinite alternate
    `};
`
export default ToTop
