import React from 'react'
import styled from '@xstyled/styled-components'
import Text from 'components/Text'

import Nav from './Nav'
import logo from '../../logo.svg'

function Header() {
  return (
    <Container>
      <Nav />
      <Logo src={logo} alt="GAIA-X-logo" />
      <Text forwardedAs="h1" $variant="h1" mt={0} fontWeight={100}>
        Take your next decision with transparency.
      </Text>
    </Container>
  )
}

const Container = styled.header`
  align-items: center;
  background-color: athensGray;
  display: flex;
  flex-direction: column;
  height: 350px;
  min-height: 350px;
  padding-top: 88px;
`
const Logo = styled.img`
  margin-bottom: 40px;
  height: 115px;
`

export default Header
