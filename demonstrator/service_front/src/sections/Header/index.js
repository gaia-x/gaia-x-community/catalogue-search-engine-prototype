import React from 'react'

import Heading from './Heading'
import Search from './Search'

function Header() {
  return (
    <>
      <Heading />
      <Search />
    </>
  )
}

export default Header
