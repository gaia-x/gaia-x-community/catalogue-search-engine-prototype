import React from 'react'
import styled from '@xstyled/styled-components'
import Link from '@material-ui/core/Link'

function Nav() {
  return (
    <Container>
      <ul style={{ margin: 0, padding: 0, listStyle: 'none' }}>
        <li>
          <Link href="/doc/api/" target="_blank" rel="noopener noreferrer">
            Guidelines and Documentation
          </Link>
        </li>
      </ul>
    </Container>
  )
}

const Container = styled.nav`
  width: fit-content;
  position: absolute;
  right: 0;
  top: 0;
  margin: 16px 50px 16px;
  font-size: 0.9rem;
`

export default Nav
