import React, { useState, useCallback, useMemo, useRef } from 'react'
import styled from '@xstyled/styled-components'
import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment'
import Chip from '@material-ui/core/Chip'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import CancelIcon from '@material-ui/icons/Cancel'

import theme from 'helpers/theme'
import useParser from 'hooks/useParser'

import Submit from './Submit'

const checkedIcon = <CheckCircleIcon style={{ color: theme.colors.emerald }} />
const errorIcon = <CancelIcon style={{ color: theme.colors.roman }} />

function getExpectedSyntax({ expected = [] } = {}) {
  return [...new Set(expected)]
    .filter(({ text = '' } = {}) => Boolean(text))
    .map(({ text } = {}) => text)
}

function Query({
  relation: currentRelation = '',
  handleValidQuery = () => {},
  handleSubmit = () => {},
}) {
  const { parse } = useParser()
  const [error, setError] = useState(parse({ string: '', onlyError: true }))
  const [query, setQuery] = useState('')
  const pressedKey = useRef()

  const handleParse = useCallback(
    (value) => {
      const trimedValue = value.replace(/  +/g, ' ')
      setQuery(trimedValue)
      const { relation, error } = parse({ string: trimedValue })
      if (relation) {
        handleValidQuery(JSON.stringify(relation))
        setError()
        return
      }
      if (currentRelation) handleValidQuery('')
      setError(error)
    },
    [currentRelation, handleValidQuery, setError, parse]
  )

  const handleReset = useCallback(() => {
    if (query.length) {
      setQuery('')
      handleValidQuery('')
      setError(parse({ string: '', onlyError: true }))
    }
  }, [setQuery, query, parse, handleValidQuery])

  const buildQuery = useCallback(
    (syntax) => {
      const whiteSpace = !query.length
        ? ''
        : ['.', '(', ')', ','].includes(syntax)
        ? ''
        : ['.', '('].includes(query.slice(-1))
        ? ''
        : ' '
      const buildedQuery = `${query}${whiteSpace}${syntax}`
      pressedKey.current = undefined
      handleParse(buildedQuery)
    },
    [query, handleParse]
  )

  const syntaxes = useMemo(() => getExpectedSyntax(error), [error])

  if (syntaxes.length === 1 && pressedKey.current !== 8) {
    buildQuery(syntaxes[0])
  }

  return (
    <Container>
      <TextField
        style={{ width: '95%', marginBottom: 8 }}
        label="Enter your query"
        variant="outlined"
        error={Boolean(error)}
        value={query}
        onChange={(e) => handleParse(e.target.value)}
        onKeyDown={(e) => {
          pressedKey.current = e.keyCode
        }}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              {Boolean(error) ? errorIcon : checkedIcon}
            </InputAdornment>
          ),
        }}
      />
      <Syntaxes>
        {syntaxes.map((syntax) => (
          <Chip
            key={syntax}
            clickable
            size="small"
            color="primary"
            variant="outlined"
            label={syntax}
            onClick={() => buildQuery(syntax)}
          />
        ))}
      </Syntaxes>

      <Submit
        handleSubmit={handleSubmit}
        handleReset={handleReset}
        disabled={!Boolean(currentRelation)}
      />
    </Container>
  )
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: auto;
  min-height: 150px;
  width: 835px;
`
const Syntaxes = styled.div`
  display: flex;

  & > div {
    margin-right: 8px;
  }
`

export default Query
