import React from 'react'
import styled, { Box } from '@xstyled/styled-components'
import Button from '@material-ui/core/Button'

function Submit({
  handleSubmit = () => {},
  handleReset = () => {},
  disabled = false,
}) {
  return (
    <Container>
      <Button
        style={{ width: 200, height: 40 }}
        variant="contained"
        color="primary"
        disableElevation
        disabled={disabled}
        onClick={handleSubmit}
      >
        Search
      </Button>
      <Box ml={2}>
        <Button
          size="small"
          color="secondary"
          disableElevation
          onClick={handleReset}
        >
          Reset all
        </Button>
      </Box>
    </Container>
  )
}

const Container = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: flex-end;
`

export default Submit
