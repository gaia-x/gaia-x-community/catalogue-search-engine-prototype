import React, { useCallback, useMemo, useState } from 'react'
import styled from '@xstyled/styled-components'
import Chip from '@material-ui/core/Chip'

import useParser from 'hooks/useParser'

import Select from 'components/Select'

import Submit from './Submit'

const CATEGORIES = [
  'Service',
  'Location',
  'Protocol',
  'Regulation',
  'Specification',
]

function reduceSelectors(opts = []) {
  return opts.reduce((acc, curr) => {
    if (CATEGORIES.includes(curr.field)) {
      return [
        ...acc,
        {
          label: curr.field,
          options: [
            ...curr.options.map((item) => ({
              title: item,
              value: item,
            })),
          ],
        },
      ]
    }
    return acc
  }, [])
}

function Selectors({
  selectorsOptions,
  relation,
  handleSubmit: handleSearchSubmit = () => {},
}) {
  const [opts, setOpts] = useState({})
  const { parse } = useParser()

  const selectors = useMemo(() => reduceSelectors(selectorsOptions), [
    selectorsOptions,
  ])
  const hasValues = useMemo(
    () => Object.values(opts).some((arr) => Boolean(arr.length)),
    [opts]
  )

  const handleSelectOption = useCallback(
    (newValues, label) => {
      setOpts({
        ...opts,
        [label]: [...newValues],
      })
    },
    [opts]
  )
  const handleRemoveOpt = useCallback(
    (value, label) => {
      setOpts({
        ...opts,
        [label]: opts[label].filter(({ title }) => title !== value),
      })
    },
    [opts]
  )
  const handleReset = useCallback(() => {
    if (hasValues) {
      setOpts({})
    }
  }, [hasValues, setOpts])

  const handleSubmit = useCallback(() => {
    const keys = Object.entries(opts)
      .filter(([label, arr]) => Boolean(arr.length))
      .map(([label]) => label)

    const query = keys.reduce((acc, curr, index) => {
      let base = index === 0 ? 'Service' : ' AND Service'

      const { property = '', edge = '' } =
        selectorsOptions.find((selector) => selector.field === curr) || {}
      if (property) {
        base = `${base}.${property} IN ANY(`
      } else {
        base = `${base} ${edge} ALL(`
      }

      const labelOpts = opts[curr]
      base = `${base}${labelOpts.map((opt, index, arr) => `'${opt.title}'`)})`

      return `${acc}${base}`
    }, '')

    const { relation } = parse({ string: query })

    handleSearchSubmit(JSON.stringify(relation))
  }, [opts, selectorsOptions, parse, handleSearchSubmit])

  return (
    <Container>
      <Selects>
        {selectors.map((item, index) => (
          <Select
            key={index}
            multiple
            value={opts[item.label]}
            handleOnChange={(values) => handleSelectOption(values, item.label)}
            {...item}
          />
        ))}
      </Selects>

      <Chips>
        {Object.entries(opts).map(([label, items]) =>
          items.map((item, index) => (
            <Chip
              key={item.title}
              label={item.title}
              onDelete={() => handleRemoveOpt(item.title, label)}
              color="primary"
            />
          ))
        )}
      </Chips>
      <Submit
        handleSubmit={handleSubmit}
        handleReset={handleReset}
        disabled={!hasValues}
      />
    </Container>
  )
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: auto;
  min-height: 150px;
  width: 835px;
`

const Selects = styled.div`
  display: flex;
  flex: 1;
  width: 100%;
  justify-content: space-between;
  margin-bottom: 16px;
  & > div {
    margin-right: 17px;
  }
`

const Chips = styled.div`
  height: auto;
  min-height: 38px;
  margin-bottom: 24px;
  & > div {
    margin-right: 8px;
  }
`

export default Selectors
