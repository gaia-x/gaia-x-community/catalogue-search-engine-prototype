import React, { useState, useCallback, useMemo } from 'react'
import { scroller } from 'react-scroll'

import styled, { css, keyframes } from '@xstyled/styled-components'
import Button from '@material-ui/core/Button'
import SwapHorizIcon from '@material-ui/icons/SwapHoriz'

import initParser from 'parser'
import api from 'api'

import useApi from 'hooks/useApi'
import { useSearch } from 'contexts/Search'

import Selectors from './Selectors'
import Query from './Query'

const modeSection = {
  selectors: (props) => <Selectors {...props} />,
  query: ({ selectorsOptions, ...props }) => <Query {...props} />,
}

function Search() {
  const { isLoading: isLoadingParser } = initParser()

  const [selectorsOptions, { isLoading: isLoadingSelectors }] = useApi({
    load: api.selectors.get,
    initialData: [],
  })

  const { loadNodes, relation, setRelation } = useSearch()

  const [mode, setMode] = useState('selectors')

  const handleSubmit = useCallback(
    async (params) => {
      await loadNodes(params)
      scroller.scrollTo('section-list', {
        duration: 800,
        delay: 200,
        smooth: true,
        offset: -60,
      })
    },
    [loadNodes]
  )
  const handleValidQuery = useCallback((rel = '') => setRelation(rel), [
    setRelation,
  ])
  const toggleMode = useCallback(() => {
    setMode(mode === 'query' ? 'selectors' : 'query')
    handleValidQuery('')
  }, [setMode, mode, handleValidQuery])

  const isLoaded = useMemo(
    () => [isLoadingSelectors, isLoadingParser].every((bool) => !bool),
    [isLoadingSelectors, isLoadingParser]
  )

  return !isLoaded ? (
    <Container $isLoaded={isLoaded}>
      <div>Loading...</div>
    </Container>
  ) : (
    <Container $isLoaded={isLoaded} id="section-search">
      <Filtering>
        {(modeSection[mode] || modeSection['selectors'])({
          relation,
          handleSubmit,
          handleValidQuery,
          selectorsOptions,
        })}
        <Switch>
          <Button
            size="small"
            color="primary"
            startIcon={<SwapHorizIcon color="primary" />}
            onClick={toggleMode}
          >
            {mode === 'query' ? 'Basic' : 'Advanced'}
          </Button>
        </Switch>
      </Filtering>
    </Container>
  )
}

const fadeIn = keyframes`
  0% {
      opacity: 0;
    }
  100% {
    opacity: 1;

  }
`

const Container = styled.section`
  display: flex;
  flex-direction: column;
  background-color: white;
  border: 1px transparent solid;
  border-color: mischka;
  border-radius: 4px;
  height: auto;
  margin: 0 auto;
  position: relative;
  width: auto;
  max-width: min-content;
  padding: 16px;
  transform: translate(0, -50%);
  transition: height 350ms ease, box-shadow 350ms ease;

  ${({ $isLoaded }) =>
    $isLoaded &&
    css`
      animation: ${() =>
        css`
          ${fadeIn} 1s ease forwards
        `};
    `}

  ${({ $isLoaded }) =>
    !$isLoaded &&
    css`
      width: fit-content;
      max-width: unset;
      min-height: 60px;
      align-items: center;
      justify-content: center;
    `}

  :hover {
    box-shadow: 0px 0px 13px -1px rgba(207, 211, 225, 1);
  }
`

const Filtering = styled.div`
  display: flex;
`

const Switch = styled.div`
  height: fit-content;
`

export default Search
