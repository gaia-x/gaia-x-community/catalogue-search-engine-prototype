#!/bin/sh

cat <<EOF
apiVersion: batch/v1
kind: Job
metadata:
  name: load-data
spec:
  template:
    spec:
      imagePullSecrets:
      - name: ci-registry
      containers:
      - name: load-data
        image: ${CI_REGISTRY_IMAGE}/neo4j_data:${CI_COMMIT_REF_SLUG}-latest
        env:
          - name: NEO4J_USERNAME
            value: neo4j
          - name: NEO4J_PASSWORD
            valueFrom:
              secretKeyRef:
                name: db-neo4j-secrets
                key: neo4j-password
          - name: NEO4J_HOST
            value: db-neo4j
      restartPolicy: Never
  # backoffLimit: 1
EOF
