// DO NOT RENAME THIS FILE
// scripts execute files in alphabetical order so this is the first one to clean the DB

MATCH (n) DETACH DELETE n;
MATCH (n) RETURN count(n) AS count;
