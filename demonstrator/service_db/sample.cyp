// ------------------------- COMMON -----------------------------

CREATE (FRA:Location {name:'France', type: 'country', code: 'FRA'})
CREATE (DEU:Location {name:'Germany', type: 'country', code: 'DEU'})
CREATE (GBR:Location {name:'United Kingdom', type: 'country', code: 'GBR'})
CREATE (POL:Location {name:'Poland', type: 'country', code: 'POL'})
CREATE (CDN:Location {name:'Canada', type: 'country', code: 'CDN'})
CREATE (NLD:Location {name:'Netherlands', type: 'country', code: 'NLD'})
CREATE (EEA:Location {name:'European Economic Area', type: 'union', code: 'EEA'})



CREATE (FRA)-[:LOCATED_IN {}]->(EEA),
       (DEU)-[:LOCATED_IN {}]->(EEA),
       (GBR)-[:LOCATED_IN {}]->(EEA),
       (POL)-[:LOCATED_IN {}]->(EEA),
       (NLD)-[:LOCATED_IN {}]->(EEA)

CREATE (swift:Protocol {name: 'SWIFT'})
CREATE (s3:Protocol {name: 'S3'})
CREATE (okoro:Protocol {name: 'OKORO'})

CREATE (GDPR:Regulation {name:'GDPR', long_name: 'General Data Protection Regulation'})
CREATE (SNC:Regulation {name:'SNC', long_name: 'ANSSI SecNumCloud Secvice Qualification'})
CREATE (HDS:Regulation {name:'HDS', long_name: 'Hébergement Données de Santé - French Health Qualification'})
CREATE (CloudAct:Regulation {name: 'CLOUD Act', long_name:'Clarifying Lawful Overseas Use of Data Act'})
// CREATE (PCIDSS:Standard {name: 'PCI DSS', long_name:'Payment Card Industry Data Security Standard'})
CREATE (ISO27001:Specification {name: 'ISO27001', long_name:'ISO/IEC 27001:2005'})
CREATE (ISO14641_1:Specification {name: 'ISO14641-1', long_name:'ISO 14641-1, Electronic archiving—Part 1'})

// ------------------------- OVH ----------------------------- 

CREATE (ovh:Provider {name:'OVHcloud', created_in: 1999, logo: 'ovhcloud-512.png', website: 'https://www.ovh.ie/'})

CREATE (ovh)-[:LOCATED_IN {}]->(FRA)
CREATE (ovh)-[:COMPLIES_WITH]->(GDPR)

CREATE (ovh_object_storage:Storage:Service {
    name:'Object Storage',
    type: 'object storage',
    logo: 'object-storage-512.png',
    website: 'https://www.ovhcloud.com/en-ie/public-cloud/object-storage/',
    backend_stack: 'openstack',
    tenancy: 'multi',
    security: 'ACL',
    high_availabity: true}
    )

CREATE (ovh_object_storage)-[:HOSTED_BY {}]->(ovh)

CREATE (ovh_object_storage)-[:IMPLEMENTS {}]->(swift),
       (ovh_object_storage)-[:IMPLEMENTS {}]->(s3)

CREATE (ovh_object_storage)-[:LOCATED_IN {}]->(FRA),
       (ovh_object_storage)-[:LOCATED_IN {}]->(CDN),
       (ovh_object_storage)-[:LOCATED_IN {}]->(DEU),
       (ovh_object_storage)-[:LOCATED_IN {}]->(GBR),
       (ovh_object_storage)-[:LOCATED_IN {}]->(POL)

// ------------------------- DOCAPOSTE -----------------------------

CREATE (docaposte:Provider {name:'DOCAPOSTE', created_in: 2009, logo: 'docaposte-512.png', website: 'https://www.docaposte.com/en/'})

CREATE (docaposte)-[:LOCATED_IN {}]->(FRA)
CREATE (docaposte)-[:COMPLIES_WITH]->(GDPR),
       (docaposte)-[:COMPLIES_WITH]->(ISO27001),
       (docaposte)-[:COMPLIES_WITH]->(ISO14641_1)

CREATE (docaposte_object_storage:Storage:Service {
    name:'Object Storage',
    type: 'object storage',
    logo: 'object-storage-512.png',
    backend_stack: 'openstack',
    backend_storage: 'NVMe / HDD',
    tenancy: 'multi',
    security: 'ACL',
    high_availabity: true}
    )
 
CREATE (docaposte_object_storage)-[:HOSTED_BY {}]->(docaposte)
CREATE (docaposte_object_storage)-[:IMPLEMENTS {security: 'HTTPS'}]->(swift),
       (docaposte_object_storage)-[:IMPLEMENTS {security: 'HTTPS'}]->(s3)
CREATE (docaposte_object_storage)-[:LOCATED_IN {}]->(FRA)

CREATE (docaposte_archive_storage:Storage:Service {
    name:'Archive Storage',
    type: 'archive storage',
    backend_stack: 'OKORO',
    backend_storage: 'SSD / HDD',
    tenancy: 'multi',
    security: 'ACL',
    high_availabity: true}
)

CREATE (docaposte_archive_storage)-[:HOSTED_BY {}]->(docaposte)
CREATE (docaposte_archive_storage)-[:IMPLEMENTS {security: 'HTTPS'}]->(okoro)
CREATE (docaposte_archive_storage)-[:LOCATED_IN {}]->(FRA)

// ------------------------- DT AG -----------------------------

CREATE (telekom:Provider {name:'Open Telelom Cloud', created_in: 2012, logo: 'telekom-512.png', website: 'https://open-telekom-cloud.com/de'})

CREATE (telekom)-[:LOCATED_IN {}]->(DEU)
CREATE (telekom)-[:COMPLIES_WITH]->(GDPR),
       (telekom)-[:COMPLIES_WITH]->(ISO27001)

CREATE (telekom_object_storage:Storage:Service {
    name:'Object Storage',
    type: 'object storage',
    type: 'archive',
    logo: 'object-storage-512.png',
    website: 'https://open-telekom-cloud.com/de',
    backend_stack: 'openstack',
    backend_storage: 'SSD / HDD',
    tenancy: 'multi',
    security: 'ACL',
    high_availabity: true}
    )

 
CREATE (telekom_object_storage)-[:HOSTED_BY {}]->(telekom)
CREATE (telekom_object_storage)-[:IMPLEMENTS {security: 'HTTPS'}]->(swift),
       (telekom_object_storage)-[:IMPLEMENTS {security: 'HTTPS'}]->(s3)
CREATE (telekom_object_storage)-[:LOCATED_IN {}]->(DEU)

CREATE (telekom_compute:Compute:Service {
    name:'Compute',
    type: 'Bare Metal',
    logo: 'object-storage-512.png',
    website: 'https://open-telekom-cloud.com/de',
    backend_stack: 'openstack',
    backend_storage: 'SSD / HDD',
    tenancy: 'multi',
    security: 'ACL',
    high_availabity: true}
    )
CREATE (telekom_compute)-[:HOSTED_BY {}]->(telekom)
CREATE (telekom_compute)-[:LOCATED_IN {}]->(DEU)


// ------------------------- INNOVO -----------------------------

CREATE (innovo:Provider {name:'iNNOVO Cloud', created_in: 2012, logo: 'innovocloud-512.png', website: 'https://www.innovo-cloud.de/'})

CREATE (innovo)-[:LOCATED_IN {}]->(DEU)
CREATE (innovo)-[:COMPLIES_WITH]->(GDPR),
       (innovo)-[:COMPLIES_WITH]->(ISO27001)

CREATE (innovo_object_storage:Storage:Service {
    name:'Object Storage',
    type: 'object storage',
    logo: 'object-storage-512.png',
    website: 'https://www.innovo-cloud.de/openstack-docs/',
    backend_stack: 'openstack',
    backend_storage: 'SSD / HDD',
    tenancy: 'multi',
    security: 'ACL',
    high_availabity: true}
    )
 
CREATE (innovo_object_storage)-[:HOSTED_BY {}]->(innovo)
CREATE (innovo_object_storage)-[:IMPLEMENTS {security: 'HTTPS'}]->(swift),
       (innovo_object_storage)-[:IMPLEMENTS {security: 'HTTPS'}]->(s3)
CREATE (innovo_object_storage)-[:LOCATED_IN {}]->(DEU)

// ------------------------- OUTSCALE -----------------------------

CREATE (outscale:Provider {name:'OUTSCALE', created_in: 2010, logo: '', website: 'https://fr.outscale.com/'})

CREATE (outscale)-[:LOCATED_IN {}]->(FRA)
CREATE (outscale)-[:COMPLIES_WITH]->(GDPR),
       (outscale)-[:COMPLIES_WITH]->(SNC),
       (outscale)-[:COMPLIES_WITH]->(HDS),
       (outscale)-[:COMPLIES_WITH]->(ISO27001)

CREATE (osc_object_storage:Storage:Service {
    name:'Object Storage',
    type: 'object storage',
    logo: 'object-storage-512.png',
    website: 'https://fr.outscale.com/solutions-stockage-cloud/stockage-objet/',
    backend_stack: 'TINA',
    backend_storage: 'SSD / HDD',
    tenancy: 'multi',
    security: 'ACL',
    high_availabity: true}
    )

CREATE (osc_object_storage)-[:HOSTED_BY {}]->(outscale)
CREATE (osc_object_storage)-[:IMPLEMENTS {security: 'HTTPS'}]->(s3)
CREATE (osc_object_storage)-[:LOCATED_IN {}]->(FRA)

// ------------------------- Orange Business Services -----------------------------

CREATE (obs:Provider {name:'Orange Business Services', created_in: 2017, logo: '', website: 'https://cloud.orange-business.com/en/offers/infrastructure-iaas/public-cloud/'})

CREATE (obs)-[:LOCATED_IN {}]->(FRA)

CREATE (obs)-[:COMPLIES_WITH]->(GDPR),
       (obs)-[:COMPLIES_WITH]->(ISO27001),
       (obs)-[:COMPLIES_WITH]->(HDS)
       

CREATE (obs_object_storage:Storage:Service {
    name:'Object Storage',
    type: 'object storage',
    logo: 'object-storage-512.png',
    website: 'https://cloud.orange-business.com/en/offers/infrastructure-iaas/public-cloud/features/object-storage-service/',
    backend_stack: 'openstack',
    backend_storage: 'SSD / HDD',
    tenancy: 'multi',
    security: 'ACL',
    high_availabity: true}
    )
 
CREATE (obs_object_storage)-[:HOSTED_BY {}]->(obs)
CREATE (obs_object_storage)-[:IMPLEMENTS {security: 'HTTPS'}]->(s3)
CREATE (obs_object_storage)-[:LOCATED_IN {}]->(FRA)
CREATE (obs_object_storage)-[:LOCATED_IN {}]->(NLD)

// Scaleway

CREATE (scaleway:Provider {name:'Scaleway', created_in: 1999, logo: 'scaleway-512.png', website: 'https://www.scaleway.com/'})

CREATE (scaleway)-[:LOCATED_IN {}]->(FRA)
CREATE (scaleway)-[:COMPLIES_WITH]->(GDPR),
       (scaleway)-[:COMPLIES_WITH]->(ISO27001),
       (scaleway)-[:COMPLIES_WITH]->(HDS)

CREATE (scaleway_object_storage:Storage:Service {
    name:'Object Storage',
    type: 'object storage',
    logo: 'object-storage-512.png',
    website: 'https://www.scaleway.com/en/object-storage/',
    backend_stack: 's3',
    backend_storage: 'SSD / NVMe / HDD',
    high_availabity: true}
    )

CREATE (scaleway_object_storage)-[:HOSTED_BY {}]->(scaleway)

CREATE (scaleway_object_storage)-[:IMPLEMENTS {security: 'HTTPS'}]->(s3)

CREATE (scaleway_object_storage)-[:LOCATED_IN {}]->(FRA),
       (scaleway_object_storage)-[:LOCATED_IN {}]->(NLD)


// ------------------------------------------------------
; // MUST KEEP SEMI COLON for cypher-shell
MATCH (n) RETURN count(n) AS count;

// MATCH (a:Provider),(b:Location) WHERE a.name = 'GAFAM' AND b.code = 'USA' CREATE (a)-[:LOCATED_IN]->(b)
// MATCH (a:Provider),(b:Regulation) WHERE a.name = 'GAFAM' AND b.name = 'CLOUD Act' CREATE (a)-[:COMPLIES_WITH]->(b)
