#!/bin/sh

set -ex

export NEO4J_HOST=${NEO4J_HOST:-localhost}
export NEO4J_ADDRESS=bolt://${NEO4J_HOST}:7687

echo "Using db ${NEO4J_HOST} with user ${NEO4J_USERNAME}"

wget --tries=5 --waitretry=2 -O /dev/null http://${NEO4J_HOST}:7474 || sleep 10
wget --tries=5 --waitretry=2 -O /dev/null http://${NEO4J_HOST}:7474 || sleep 10
wget --tries=5 --waitretry=2 -O /dev/null http://${NEO4J_HOST}:7474

for f in /cyphers/*.cyp; do
    echo "running cypher ${f}"
    cypher-shell --address ${NEO4J_ADDRESS} --file $f
done
