# S3 Capabilities evaluation

Based on https://github.com/ceph/s3-tests

Each provider will provide output of the following command:

```
S3TEST_CONF=./s3tests.conf ./virtualenv/bin/nosetests -v --with-xunit
```

Output file is to be pushed as *nosetest-provider.xml*
