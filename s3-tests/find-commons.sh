#!/bin/sh

for var in HOME; do
    [ -z $( eval "echo \$$var" ) ] && { echo "Variable $var is undefined."; return 1; }
done

which xq > /dev/null 2>&1 || { echo "try pip install yq first"; return 1; }

BLACK=0
RED=1
GREEN=2
YELLOW=3
BLUE=4
MAGENTA=5
CYAN=6
WHITE=7
GRAY=8

title() {
    tput bold || true
    tput setaf $CYAN || true
    echo $1
    tput sgr0 || true
}

info() {
    tput setaf $GREEN || true
    echo $1
    tput sgr0 || true
}

count=$(ls -1 nosetests-*.xml | wc --lines)

info "Found $count result files"

for file in nosetests-*.xml; do
    title=$(echo ${file} | cut -d- -f2- | cut -d. -f-1)
    title $title
    count_test=$(cat $file | xq -r '.testsuite["@tests"]')
    echo "tests: ${count_test}"
    count_error=$(cat $file | xq -r '.testsuite["@errors"]')
    echo "error: ${count_error}"
    count_failure=$(cat $file | xq -r '.testsuite["@failures"]')
    echo "failure: ${count_failure}"
    count_skip=$(cat $file | xq -r '.testsuite["@skip"]')
    echo "skip: ${count_skip}"
    count_success=$(($count_test - $count_error - $count_failure - $count_skip))
    echo "success: ${count_success}"
    cat $file | xq -r '.testsuite.testcase[] | select(has("error") or has("failure") or has("skipped")| not) | .["@classname"] + "." + .["@name"]' > tests-success-$title.txt
    cat $file | xq -r '.testsuite.testcase[] | select(has("error")) | .["@classname"] + "." + .["@name"]' > tests-error-$title.txt
    cat $file | xq -r '.testsuite.testcase[] | select(has("failure")) | .["@classname"] + "." + .["@name"]' > tests-failure-$title.txt
    cat $file | xq -r '.testsuite.testcase[] | select(has("skipped")) | .["@classname"] + "." + .["@name"]' > tests-skipped-$title.txt
done

cat tests-success-*.txt | sort | uniq --count | sed --quiet "s/^ \+$count\+ \(.*\)$/\1/p" > common-tests-success.txt

info "Found $(cat common-tests-success.txt | wc --lines) common tests"
