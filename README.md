# This repository is an archive of the demonstrator from June 4th 2020.
# It's superseeded by https://gitlab.com/gaia-x/gaia-x-core/gaia-x-portal

---

# Cross-EU Souvereign Data Demonstrator

GAIA-X France German Demonstrator for Hanover fair

[![pipeline status](https://gitlab.com/gaia-x/gaia-x-core/cross-eu-souvereign-data-demonstrator/badges/master/pipeline.svg)](https://gitlab.com/gaia-x/gaia-x-core/cross-eu-souvereign-data-demonstrator/-/commits/master)

# Demonstrator

## Staging environment

https://staging.gaia-x-demonstrator.eu/

## API docs

TBD

# Overall demonstrator scenario

![scenario](demonstrator/sequence_diagram.png "Demonstrator Scenario")


# S3 documentation

## Deutsche Telekom

- Product description: https://open-telekom-cloud.com/en/products-services/object-storage-service
- Documentation: https://open-telekom-cloud.com/en/support/tutorials/automating-opentelekomcloud-apis

## OVHcloud

- Product description: https://www.ovhcloud.com/en-gb/public-cloud/object-storage/
- Documentation: https://docs.ovh.com/gb/en/public-cloud/getting_started_with_the_swift_S3_API/

## Scaleway

- Product description: https://www.scaleway.com/en/object-storage/
- Documentation: https://www.scaleway.com/en/docs/object-storage-with-s3cmd/

## iNNOVO Cloud / German Edge Cloud

- Product description: https://www.innovo-cloud.de/openstack-docs/
- Documentation: https://docs.openstack.org/swift/latest/api/object_api_v1_overview.html

## 3DS OUTSCALE

- Product description: https://en.outscale.com/storage/osu-object-storage/
- Documentation: https://wiki.outscale.net/display/EN/About+OSU

## Orange Business Services 

- Production description: https://cloud.orange-business.com/en/offers/infrastructure-iaas/public-cloud/features/object-storage-service/
- Documentation: https://docs.prod-cloud-ocb.orange-business.com/ugs3cmd/obs/en-us_topic_0051518473.html

# Licence

Under [BSD 3-Clause License](LICENCE)
