# Platform setup for the GAIA-X demonstrator

The platform contains

- a managed kubernetes cluster
- a Gitlab with:
  - a repository
  - kubernetes integration
  - a private registry
  - configured CI/CD pipelines
- centralized log system with Fluentd
- Cert-manager

---
NOTE: **Kubernetes version is 1.17**

---


# Kubernetes cluster configuration

## Gitlab integration

Follow the steps on the gitlab cluster page.

The used yaml files are available here:
- [gitlab-admin-service-account.yaml](gitlab-admin-service-account.yaml)

## Configuring Fluent

The documnetation is available here https://docs.ovh.com/gb/en/logs-data-platform/kubernetes-fluent-bit/

The used yaml files are available here:
- [fluent-bit-configmap.yaml](fluent-bit-configmap.yaml)
- [fluent-bit-ds.yaml](fluent-bit-ds.yaml)

The graylog server is available at

```shell
kubectl get daemonset -n logging -l k8s-app=fluent-bit-logging -o json | jq '.items[0].spec.template.spec.containers[0].env[] | select(.name == "FLUENT_LDP_HOST").value'
```

## Cert-manager

### Cert-manager installation

Following this documentation https://cert-manager.io/docs/installation/kubernetes/ for Helm 3 

```shell
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.14.3/cert-manager.crds.yaml
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v0.14.3
```

### Issuer configuration

Following this documentation https://cert-manager.io/docs/tutorials/acme/ingress/, replacing `Issuer` by `ClusterIssuer`

The used yaml files are available here:
- [cert-manager-clusterissuer.yaml](cert-manager-clusterissuer.yaml)

```shell
kubectl describe clusterissuer letsencrypt-staging
kubectl describe clusterissuer letsencrypt-prod
```

## Development

Gitlab CI/CD is configured to run on every branch but to create an environment only for branches following the syntax `dev/xxx`.


## Neo4j database

The neo4j database is installed in each environment with options from [values.yaml](/servce_db/values.yaml)

---
NOTE: **The community edition is used, so only 1 core and no readReplica are used.**

---

### Accessing the DB in the cluster

You can access the DB localy when forwarding port like

```shell
while true; do
    sleep 1
    kubectl port-forward -n neo4j service/db-neo4j 7474:7474 7687:7687
done
```

If working on a remote bastion, combine above command with SSH remote port forwarding to use local Neo4j browser application

```shell
ssh -v -L 7474:localhost:7474 -L 7687:localhost:7687 <bastion>
```

You can also run a shell with Neo4j tools with

```shell
kubectl run -it -n <namespace> --image=neo4j:4.0.3 neo4j-shell bash
```

## Debug

To enter a pod

```shell
kubectl exec -it -n <namespace> <pod name>  -- /bin/bash 
```
